<?php

$posted = false;

if(
	!empty($_POST['ShortName']) &&
	!empty($_POST['favicon']) &&
	!empty($_POST['searchUrl'])
){
	$posted = true;
	$fp = fopen('opensearch.xml', 'w');

	$_POST['favicon'] = htmlEntities($_POST['favicon']);
	$_POST['searchUrl'] = htmlEntities($_POST['searchUrl']);

	fwrite($fp, "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<OpenSearchDescription xmlns=\"http://a9.com/-/spec/opensearch/1.1/\">
	<ShortName>{$_POST['ShortName']}</ShortName>");

	if(!empty($_POST['LongName'])){
		fwrite($fp, "\n\t<LongName>{$_POST['LongName']}</LongName>");
	}

	if(!empty($_POST['Description'])){
		fwrite($fp, "\n\t<Description>{$_POST['Description']}</Description>");
	}

	fwrite($fp, "\n\t<InputEncoding>UTF-8</InputEncoding>
	<Image>{$_POST['favicon']}</Image>");

	if(!empty($_POST['postVariable'])){
		fwrite($fp, "\n<Url type=\"text/html\" method=\"post\" template=\"{$_POST['searchUrl']}\">
		<Param name=\"{$_POST['postVariable']}\" value=\"{searchTerms}\" />
	</Url>");
	} else {
		fwrite($fp, "\n\t<Url type=\"text/html\" method=\"get\" template=\"{$_POST['searchUrl']}\" />");
	}

	fwrite($fp, "\n</OpenSearchDescription>");
	fclose($fp);
}

?>

<html>

<head>

<style>
<!--

table tr th {
	text-align: left;
}

//-->
</style>

<?php if($posted){ ?>
<link rel="icon" href="<?php echo $_POST['favicon']; ?>" />
<link rel="shortcut icon" href="<?php echo $_POST['favicon']; ?>" />
<link title="<?php echo $_POST['ShortName']; ?>" type="application/opensearchdescription+xml" rel="search" href="opensearch.xml" />
<?php } ?>

</head>

<body>

<form method="POST">

<table>
<tr>
<th>ShortName:</th>
<td><input type="text" name="ShortName" value="<?php if(!empty($_POST['ShortName'])) echo $_POST['ShortName']; ?>" /></td>
</tr>
<tr>
<th>LongName (optional):</th>
<td><input type="text" name="LongName"  value="<?php if(!empty($_POST['LongName'])) echo $_POST['LongName']; ?>" /></td>
</tr>
<tr>
<th>Description (optional):</th>
<td><input type="text" name="Description" value="<?php if(!empty($_POST['Description'])) echo $_POST['Description']; ?>" /></td>
</tr>
<tr>
<th>favicon:</th>
<td><input type="text" name="favicon" value="<?php if(!empty($_POST['favicon'])) echo $_POST['favicon']; ?>" /></td>
</tr>
<tr>
<th>Search URL:</th>
<td><input type="text" name="searchUrl" value="<?php if(!empty($_POST['searchUrl'])) echo $_POST['searchUrl']; ?>" /></td>
</tr>
<tr>
<th>Post String Variable (optional):</th>
<td><input type="text" name="postVariable" value="<?php if(!empty($_POST['postVariable'])) echo $_POST['postVariable']; ?>" /></td>
</tr>
<tr>
<td colspan="2">
<input type="submit" />
</td>
</tr>
</table>

</form>

</body>

</html>
