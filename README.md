opensearchgen 1.0.0
===================

Generate custom open search XML documents, since Firefox doesn't let you edit them inside the browser.
